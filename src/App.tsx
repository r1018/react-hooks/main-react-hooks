import Demo from "./hooks/Demo";

function App() {
  return (
    <div className="App">
      <h1>Space Trip</h1>
      <Demo />
    </div>
  );
}

export default App;
