import React, { useState, useEffect, useMemo } from "react";
import { Form, Button, Container, Row, Stack } from "react-bootstrap";

const EMAIL_REGEX =
  /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;

// 3 most important hooks - useState, useEffect, useMemo
const Demo = () => {
  const [isChecked, setChecked] = useState(false);
  const [email, setEmail] = useState("");

  const clickedOnBody = () => {
    alert("Clicked on body");
  };

  // useEffect with no dependencies tells React to run only once on componentDidMount - mounts and renders for the first time
  // componentDidMount() - intialization, adding event listeners to the dom - events you want your users to have onMount
  // (1) - 1st functionality - initalizing
  //   useEffect(() => {
  //     alert("Mounting component");
  //   }, []);

  // (2) - 2nd functionality - adding event listeners
  //   useEffect(() => {
  //     document.body.addEventListener("click", clickedOnBody);
  //   }, []);

  // (3) - 3rd functionality - onComponentWillMount() - dispose any event listeners you have for components
  // explicitly remove events
  // willMount  - cleanup function
  //   useEffect(() => {
  //     // called whenever the component is inMounted
  //     return () => {
  //       document.body.removeEventListener("click", clickedOnBody);
  //     };
  //   }, []);

  // (4) - keeping track of a particular state variable - reacting to any change that goes through the variable
  // called anytime the variable changes
  // didUpdate
  //   useEffect(() => {
  //     const isValidEmail = EMAIL_REGEX.test(email);
  //     if (!isValidEmail) {
  //       console.log("Please enter valid email");
  //     }
  //   }, [email]);

  function fibonacci(n: number): number {
    return n < 1 ? 0 : n <= 2 ? 1 : fibonacci(n - 1) + fibonacci(n - 2);
  }

  // fibonacci of 20 is quite a big number - takes quite a bit of time to calculate the function
  //   const result = fibonacci(20);

  //   const result = useMemo(() => fibonacci(20), []);
  const result = useMemo(() => {
    const fib = fibonacci(20);

    console.log("Resut: ", fib);

    return fib;
  }, []);
  // we keep getting the result when state changes but it isnt getting recalculated
  //  console.log("Result: ", result);

  return (
    <Form style={{ width: "20rem", margin: "auto" }}>
      <Stack gap={3}>
        <Form.Check
          checked={isChecked}
          onChange={(event) => {
            // access the target - the taget is the dom element - access the dom element and we know if its checked or not
            const newValueOfIsChecked = event.target.checked;
            setChecked(newValueOfIsChecked);
          }}
          type="switch"
          label="Send me Email Updates"
        />
        {/* value is what will be inside the input */}
        <Form.Control
          value={email}
          onChange={(event) => {
            const newEmail = event.target.value;
            setEmail(newEmail);
          }}
          type="email"
          placeholder="Enter Your Email"
        />
        <Button>Join the Waiting List!</Button>
      </Stack>
    </Form>
  );
};

export default Demo;
