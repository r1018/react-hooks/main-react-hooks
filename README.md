<img src="https://raw.githubusercontent.com/devicons/devicon/master/icons/react/react-original-wordmark.svg" alt="react" width="40" height="40"/>

## Beginner Main React Hook Concepts

### useState, useEffect, useMemo hooks

<img src="lifecycles1.png" alt="lifecycle methods" width="500" />

[Lifecycle hooks diagram](https://projects.wojtekmaj.pl/react-lifecycle-methods-diagram/)

- Shows behind the scenes lifecycle methods - behind the scenes of what is happening in React.
- Those methods are mostly compatible/part of class components in React - dont exist in functional components
- In functional components we replace most of these methods using useEffect:

  - shouldComponentUpdate()
  - render() - basically our functional components
  - getSnapshotBeforeUpdate()
  - React updates DOM and refs

- At the end we get 3 different methods which are the binding points throughout the application.
  These methods tell us the what is the current lifecycle of the React application were running,
  so we can manage different variables, dispose and remove variables from memory.
- In general allow us to manage the application better.
  Use effect replaces these 3 methods using a single method.
  _ componentDidMount()
  _ componentDidUpdate() \* componentWillUnmount()

### USE MEMO

- performance guy that allows you to make your react apps more performant
  without noticing tiny side effects
- based on memoization - a type of caching where you first calculate the actual result,
  but later on if the input doesnt change, it basically returns the same result you got before
  because the input originally didnt change. We dont need to recalculate and can save
  performance - return the same result

- Everytime the component re-renders the large function that gets calculated will run every single time - triggers a re-render everytime
  If any variable state changes, the component will re-render and run/calculate the function everytime

- We wrap the expensive function in a useMemo, and the dependency will calculate when the value changes
  and when to re-run
  If the value wont change we only need to run the function once and can just pass in empty brackets []
  const result = useMemo(() => fibonacci(20), []);

- We keep getting the result when state changes but it isnt getting recalculated
  the result is completely cached
  console.log("Result: ", result);

- We can check to see if its not re-calculating the result by doing the following:

```
const result = useMemo(() => {
const fib = fibonacci(20);

    console.log("Resut: ", fib);

        return fib;

}, [])
```

<img src="app.png" alt="lifecycle methods" width="500" />
